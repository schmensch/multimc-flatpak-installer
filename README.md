# **You *can* use this, although I'd recommend using PolyMC instead, a FOSS GPLv3-Fork of MultiMC. They have a proper and less hacky Flatpak build available.**

# MultiMC-Flatpak-Installer

## Description
This is just a simple Flatpak, which has a script packaged that downloads and starts the latest release from https://multimc.org. We can't distribute the binaries directly due to licensing restrictions.

## Installation
This is not yet on FlatHub or any other Flatpak Repo (because I'm afraid of Peterix shitting on Flatpak again and making a takedown request), but you can build the latest release locally:

```bash
# First install flatpak and flatpak-builder, using arch as an example here
sudo pacman -S --needed flatpak flatpak-builder

# Install the necessary flatpak dependencies
flatpak install org.kde.Platform/x86_64/5.15 org.kde.Sdk/x86_64/5.15

# Build and install the flatpak locally
flatpak-builder --install --user --force-clean build-dir org.multimc.MultiMC.yml
```

## Bug reports
This is an downstream project, thus Do **NOT** open any issues related to this on the official MultiMC bug tracker, instead, open an issue in this repo.

## Contributing
I'm happy to see contributions in form of Merge Requests, issues, general feedback, ... to this project.

To get started, just clone this repo, make a new branch, and get developing. The Flatpak Documentation could be useful: https://docs.flatpak.org/en/latest

## License
This project is licensed under the MIT license. The official binaries from https://multimc.org are licensed under Apache 2.0. The JDKs included can be found on https://github.com/adoptium. They seem to be licensed under GPL.
