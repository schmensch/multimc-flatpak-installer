#include <dlfcn.h>
#include <stdbool.h>
#include <assert.h>

typedef bool (*FUNC_TYPE)(void *);

static bool stub(void *);

static FUNC_TYPE func = stub;

static bool stub(void *ptr) {
    void *handle = dlopen("libqt_override_openurl.so", RTLD_GLOBAL | RTLD_NOW);
    assert(handle);
    func = dlsym(handle, "_ZN16QDesktopServices7openUrlERK4QUrl");
    return func(ptr);
}

// Trampoline
extern bool _ZN16QDesktopServices7openUrlERK4QUrl(void *);
bool _ZN16QDesktopServices7openUrlERK4QUrl(void *ptr) {
    return func(ptr);
}
