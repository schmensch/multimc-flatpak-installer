#include <QUrl>
#include <QDesktopServices>
#include <QProcess>

bool QDesktopServices::openUrl(const QUrl &url) {
    return QProcess::execute(QLatin1String("xdg-open"), QStringList(url.toString()));
}
